<?php

namespace Database\Factories;

use App\Models\Projects;
use App\Models\projects_user;
use App\Models\User;
use Illuminate\Database\Eloquent\Factories\Factory;

class ProjectsUserFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = projects_user::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'projects_id' => function () {
                return Projects::factory()->create()->id;
            },
            'user_id' => function () {
                return User::factory()->create()->id;
            }
        ];
    }
}
