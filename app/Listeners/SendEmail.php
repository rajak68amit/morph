<?php

namespace App\Listeners;

use Illuminate\Support\Facades\Mail;
use App\Events\UserCreated;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;

class SendEmail
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  UserCreated  $event
     * @return void
     */
    public function handle(UserCreated $event)
    {
        // echo $event->email;
        $event->email;
        // Mail::to('amitkumarrajak68@gmail.com')->send();
    }
}
