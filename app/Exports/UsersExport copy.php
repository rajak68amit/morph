<?php

namespace App\Exports;

use App\Models\User;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithEvents;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithMapping;
use Maatwebsite\Excel\Events\AfterSheet;

class UsersExport implements FromCollection, WithHeadings, ShouldAutoSize, WithMapping, WithEvents   // ShouldAutoSize this use for resize data in excel and in pdf
{
    /**
    * @return \Illuminate\Support\Collection
    */
    public function collection()
    {
        return User::with('address_one')->get();
    }

    // this is for get selected data in pdf,xls
    public function map($user):array
    {
        return [
            $user->id,
            $user->name,
            $user->email,
            $user->address_one->address,
            $user->created_at,
        ];
    }

    /// for display heading
    public function headings():array
    {
        return [
            '#',
            'Name',
            'Email',
            'Country',
            'Created at'
        ];
    }

    public function registerEvents():array
    {
        return [
            AfterSheet::class => function (AfterSheet $event) {
                $event->sheet->getStyle('A1:F1')->applyFromArray([
                    'font' => [
                        'bold' => true
                    ],
                    'borders' => [
                        'outline' => [
                            'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THICK,
                            'color' => ['argb' => 'FFFF0000'],
                        ],
                    ],
                ]);
            }
        ];
    }
}
