<?php

namespace App\Exports;

use App\Models\User;
use Maatwebsite\Excel\Concerns\FromQuery;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithCustomStartCell;
use Maatwebsite\Excel\Concerns\WithDrawings;
use Maatwebsite\Excel\Concerns\WithEvents;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithMapping;
use Maatwebsite\Excel\Events\AfterSheet;
use PhpOffice\PhpSpreadsheet\Worksheet\Drawing;

class UsersExport implements
 WithHeadings, // for display heading of data
  ShouldAutoSize, // for autoresize xls colmns
  WithMapping, // for getting selected column
  WithEvents, // for making style like heading bold , color etc
  FromQuery, // for generate large data in excl
  WithDrawings, // for logo adding in xls sheet
  WithCustomStartCell  // ShouldAutoSize this use for resize data in excel and in pdf
{
    private $year;
    private $month;

    public function __construct(int $year, int $month)
    {
        $this->year = $year;
        $this->month = $month;
    }

    /**
    * @return \Illuminate\Support\Collection
    */
    public function query()
    {
        return User::query()->with('address_one')
        ->whereMonth('created_atl', $this->month)
       ->whereYear('created_at', $this->year)
        //->take(200); // query() add first because of follow fromquery method
;
    }

    // this is for get selected data in pdf,xls
    public function map($user):array
    {
        return [
            $user->id,
            $user->name,
            $user->email,
            $user->address_one->address,
            $user->created_at,
        ];
    }

    /// for display heading
    public function headings():array
    {
        return [
            '#',
            'Name',
            'Email',
            'Country',
            'Created at'
        ];
    }

    public function registerEvents():array
    {
        return [
            AfterSheet::class => function (AfterSheet $event) {
                $event->sheet->getStyle('A8:F8')->applyFromArray([
                    'font' => [
                        'bold' => true
                    ],
                    'borders' => [
                        'outline' => [
                            'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THICK,
                            'color' => ['argb' => 'FFFF0000'],
                        ],
                    ],
                ]);
            }
        ];
    }

    public function drawings()
    {
        $drawing = new Drawing();
        $drawing->setName('Logo');
        $drawing->setDescription('This is my logo');
        $drawing->setPath(public_path('/img/laravel.png'));
        $drawing->setHeight(90);
        $drawing->setCoordinates('B2');

        return $drawing;
    }

    public function startCell():string
    {
        return 'A8';
    }
}
