<?php

namespace App\Exports;

use Maatwebsite\Excel\Concerns\WithMultipleSheets;

class UserMultiSheetExport implements WithMultipleSheets
{
    private $year;

    public function __construct(int $year)
    {
        $this->year = $year;
    }

    public function sheets(): array
    {
        $sheets = [];
        for ($month = 6; $month <= 12; $month++) {
            $sheets[] = new UsersExport($this->year, $month);
        }
        return $sheets;
    }

    public function sheetsbkup(): array
    {
        $sheets = [];
        for ($month = 6; $month <= 12; $month++) {
            $sheets[] = new UsersExport($this->year, $month);
        }
        return $sheets;
    }
}
