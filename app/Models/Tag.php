<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Tag extends Model
{
    use HasFactory;
    protected $guarded =[];

    // this work in many to many relation ship
    public function basicposts()
    {
        return $this->belongsToMany(Post::class, 'post_tag', 'tag_id', 'post_id')->withTimestamps();
    }

    public function posts()
    {
        return $this->morphedByMany(Post::class, 'taggable');
    }
    public function videos()
    {
        return $this->morphedByMany(Video::class, 'taggable');
    }
}
