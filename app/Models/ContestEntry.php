<?php

namespace App\Models;

use App\Events\NewEntryReceived;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ContestEntry extends Model
{
    use HasFactory;
    protected $guarded = [];
    protected $table = 'entryreceived';

    public static function booted()
    {
        static::created(function ($contestEntry) {
            NewEntryReceived::dispatch();
        });
    }
}
