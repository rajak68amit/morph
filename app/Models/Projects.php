<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Projects extends Model
{
    use HasFactory;
    protected $guarded = [];

    public function users()
    {
        return $this->belongsToMany(User::class);
    }

    public function tasks()
    {
        return $this->hasManyThrough(
            Task::class,
            Team::class,
            'projects_id',
            'user_id',
            'id',
            'user_id'
        );
    }

    public function task()
    {
        return $this->hasOneThrough(
            Task::class,
            Team::class,
            'projects_id',
            'user_id',
            'id',
            'user_id'
        );
    }
}
