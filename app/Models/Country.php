<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Country extends Model
{
    use HasFactory;
    protected $guarded = [];

    public function state()
    {
        return $this->hasMany(State::class, 'c_id', 'id');
    }


    public function cities()
    {
        return $this->hasManyThrough(City::class, State::class, 'c_id', 's_id');
    }
}
