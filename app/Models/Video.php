<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Video extends Model
{
    use HasFactory;
    protected $guarded = [];

    public function comments() // this is one to many ploymor relation ship
    {
        return $this->morphMany(Comment::class, 'commentable'); // morphTo means in polymorphism one to many relation ship
    }
    public function comment() // this is one to one ploymor relation ship
    {
        return $this->morphOne(Comment::class, 'commentable'); // morphTo means in polymorphism one to many relation ship
    }

    public function tags()
    {
        return $this->morphToMany(Tag::class, 'taggable', 'taggables');
    }
}
