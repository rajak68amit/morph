<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Relations\Pivot;

class PostTag extends Pivot
{
    use HasFactory;
    protected $table = 'post_tag';

    public static function boot()
    {
        Parent::boot();

        static::created(function ($iteam) {
            //dd($iteam, 'created');
        });
        static::deleted(function ($iteam) {
            // dd($iteam, 'deleted');
        });
    }
}
