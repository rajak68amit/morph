<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Post extends Model
{
    use HasFactory;
    protected $guarded = [];

    public function user()
    {
        return $this->belongsTo(User::class);
    }
    // this work in many to many relation ship
    public function basictags()
    {
        // return $this->belongsToMany(Tag::class, 'post_tag', 'post_id', 'tag_id')
        // ->withTimestamps()
        return $this->belongsToMany(Tag::class, 'post_tag', 'post_id', 'tag_id')
        ->using(PostTag::class)
        ->withTimestamps()
        
        //->withPivot('status')
        ;
        /** post_tag == is table name in which store post is and tag id
         * withTimestamps == post_tag is pivot table inwhich by force insert timestamp
         * withPivot(status)=>  this is way to retrive pivote table row value
         */
    }



    public function comments()
    {
        return $this->morphMany(Comment::class, 'commentable');
    }
    public function comment()
    {
        return $this->morphOne(Comment::class, 'commentable')->latest();
    }


    public function tags()
    {
        return $this->morphToMany(Tag::class, 'taggable', 'taggables');
    }
}
