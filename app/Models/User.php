<?php

namespace App\Models;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use SebastianBergmann\CodeCoverage\Report\Xml\Project;

class User extends Authenticatable implements MustVerifyEmail
{                                   // MustVerifyEmail  => this for email veryfication
    use HasFactory, Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'email',
        'password', 'project_id'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function post()
    {
        return $this->hasOne(Post::class)->withDefault(['title' => 'Not available']);
    }

    public function address_one()
    {
        return $this->hasOne(Address::class)->withDefault(['address' => 'Not available']);
    }

    public function addresses()
    {
        return $this->hasMany(Address::class);
    }

    public function posts()
    {
        return $this->hasMany(Post::class);
    }

    public function projects()
    {
        return $this->BelongsToMany(Project::class);
    }

    public function tasks()
    {
        return $this->hasMany(Task::class);
    }
}