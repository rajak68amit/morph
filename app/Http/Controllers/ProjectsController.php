<?php

namespace App\Http\Controllers;

use App\Models\Projects;
use App\Models\Task;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use SebastianBergmann\CodeCoverage\Report\Xml\Project;

class ProjectsController extends Controller
{
    public function projectTask()
    {
        $project = Projects::find(1);

        return $project->task;
        // $project = Projects::find(1);

        // return $project->tasks;
    }

    public function indexk()
    {
        $projects =  Projects::create([
            'name'=>'Project A'
        ]);
        $project1 =  Projects::create([
            'name'=>'Project B'
        ]);


        $user3 = User::create([
            'name'=>"anurag3",
            'email'=>"anurag3@email.com",
            'password'=>Hash::make('password'),
        ]);
        $user4 = User::create([
            'name'=>"anurag4",
            'email'=>"anurag4@email.com",
            'password'=>Hash::make('password'),
        ]);
        $user5 = User::create([
            'name'=>"anurag5",
            'email'=>"anurag5@email.com",
            'password'=>Hash::make('password'),
        ]);

        $projects->users()->attach($user3);
        $projects->users()->attach($user5);
        $projects->users()->attach($user4);

        $project1->users()->attach($user3);
        $project1->users()->attach($user5);

        // Task::create([
        //     'user_id'=>$user3->id,
        //     'title'=>'Task 3 for project 2 by user 3'
        // ]);
        // Task::create([
        //     'user_id'=>$user4->id,
        //     'title'=>'Task 4 for project 2 by user 4'
        // ]);
        // Task::create([
        //     'user_id'=>$user5->id,
        //     'title'=>'Task 5 for project 2 by user 5'
        // ]);
    }

    public function show(Projects $projects)
    {
        $projects = Projects::with('users')->get();
        return view('admin.project.usersproject', compact('projects'));
    }

    public function projectsTasks(Projects $projects)
    {
        $projects = Projects::with('tasks')->get();
        return view('admin.project.usersprojecttasks', compact('projects'));
    }
}
