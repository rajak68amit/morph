<?php

namespace App\Http\Controllers;

use App\Models\Address;
use App\Models\Post;
use App\Models\User;

class AddressController extends Controller
{
    public function index()
    {
        $addresses = Address::with('user')->get();

        return view('admin.Post.user', compact('addresses'));
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function indexd()
    {
        $user1 = User::find(1);
        $user4 = User::find(4);
        $user2 = User::find(2);
        $user3 = User::find(3);
        $user5 = User::find(5);

        $Post = Post::create([
            'title' => 'noida sector one',
            'user_id' => $user4->id,
        ]);
        $Post = Post::create([
            'title' => 'noida sector five',
            'user_id' => $user1->id,
        ]);
        $Post = Post::create([
            'title' => 'new ashok nagar a block',
            'user_id' => $user2->id,
        ]);
        $Post = Post::create([
            'Post' => 'new ashok nagar a block',
            'user_id' => $user2->id,
        ]);
        $Post = Post::create([
            'title' => 'mayur vihar',
            'user_id' => $user3->id,
        ]);
        $Post = Post::create([
            'title' => 'kundali ',
            'user_id' => $user3->id,
        ]);
        $Post = Post::create([
            'title' => 'noida sector 144 ',
            'user_id' => $user5->id,
        ]);
    }
}
