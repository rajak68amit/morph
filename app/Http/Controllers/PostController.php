<?php

namespace App\Http\Controllers;

use App\Models\Comment;
use App\Models\Country;
use App\Models\Post;
use App\Models\Tag;
use App\Models\User;
use App\Models\Video;
//use Facade\FlareClient\Stacktrace\File;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Spatie\YamlFrontMatter\YamlFrontMatter;
use Illuminate\Support\Facades\File;

class PostController extends Controller
{
    // public $title;
    // public $excerpt;
    // public $date;
    // public $body;

    // public function __construct($title, $excerpt, $date, $body)
    // {
    //     $this->title = $title;
    //     $this->excerpt = $excerpt;
    //     $this->date = $date;
    //     $this->body = $body;
    // }


    public function ymail_front_matterpost()
    {
        $document = YamlFrontMatter::parseFile(
            resource_path('views/ymal_front_matter.html')
        );
    
        ddd($document->excert);
    }
    
    public function ymail_front_matter()
    {
        $file = 'views/yaml';
        $files = File::files(resource_path($file));
        $documents =[];
        foreach ($files as $file) {
            $documents[]  = YamlFrontMatter::parseFile($file);
        }
        ddd($documents);
    }
    



















    public function morphtag()
    {
        $tag = Tag::find(1); //get video post on tag base
        //return $tag->posts; //get post on tag base
        return($tag->videos);

        //return $tag;

        //$video = Video::create(['title'=>'second morph video tag']);
        //$video->tags()->create(['name'=>'video tag']); //create a tag and add to taggables table also
       // $tag = Tag::find(1); $video->tags()->attach($tag); // find tag and add to taggables table

        // $post = Post::create(['user_id'=>3,'title'=>'Title for morph']);
        // $post->tags()->create(['name'=>'New Laravel']);
    }


    public function comments()
    {
        //$video  = Video::create(['title'=>'first video']);


        // $video = Comment::find(2);

        // dd($video->subject);
        // $video->comments()->create([
        //     'user_id'=>1,
        //     'body'=>'comment for video2',
        // ]);
        
        /**  $post = Post::find(8);
        $post->comments()->create(['user_id'=>1,'body'=>'2nd comment for post']);
        return $post->comments; */


        $post = Post::find(8);
        return $post->comment;
    }
    public function commentsinsert()
    {
        $user = User::create([
            'name'=>'raju',
            'email'=>'rajud@email.com',
            'password'=>Hash::make('password'),
         
        ]);

        $post= Post::create([
            'user_id'=>$user->id,
            'title'=>"first post comment"
        ]);

        $post->comments()->create([
            'user_id'=>$user->id,
            'body'=>'comment for post',
        ]);
    }



    public function index()
    {
        $posts = Post::with('user')->get();

        return view('admin.post.index', compact('posts'));
    }

    public function posttag()
    {
        $posts = Post::with(['user','basictags'])->get();
        return view('admin.post.post_tag', compact('posts'));
    }
    public function posttagattach()
    {
        // $tag = Tag::find(1);
        // $post = Post::find(2);
        // $post->basictags()->attach($tag);
    }
    public function posttagdetach()
    {
        $tag = Tag::find(1);
        $post = Post::find(1);
        $post->basictags()->detach($tag);
    }
    public function posttagsync()
    {
        $tag = Tag::find(4);
        $post = Post::find(4);
        $post->basictags()->sync($tag);
    }

    public function tagpost()
    {
        $tags = Tag::with(['posts'])->get();
      
        return view('admin.tags.tag', compact('tags'));
    }

    public function tagpostattach()
    {
        $tag = Tag::find(1);
        $post = Post::find(3);
        $tag->posts()->attach($post);
    }

    public function tagpostdetach()
    {
        $tag = Tag::find(1);
        $post = Post::find(1);
        $post->basictags()->detach($tag);
    }
    public function tagpostsync()
    {
        $tag = Tag::find(4);
        $post = Post::find(4);
        $post->basictags()->sync($tag);
    }
}
