<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Storage;
use App\Models\Fileupload;
use Illuminate\Http\Request;
use Intervention\Image\Facades\Image;
use Illuminate\Http\File;

class FileUploadController extends Controller
{
    public function upload()
    {
        /// return Fileupload::all();
        // echo asset('storage/file.txt');
        return view('admin.upload.index');
    }


    public function uploadtodb(Request $request)
    {
        $img= $request->file('photo')
        ->isValid();// get image valid or not
        //->getError();// get error type
        // ->getSize();// get image size
        //->getClientOriginalName();// get image origional name
        //->guessExtension();// get image .extension
        // ->getMimeType();// get image type
        // ddd($img);

        // $request->validate([
        //     'photo'=>'required|mimes:png,jpg,jpeg|max:7'
        // ]);

        //if ($request->hasFile('photo')) {
        $image      = $request->file('photo');
        $fileName   = time() . '.' . $image->getClientOriginalExtension();
        $img = Image::make($image->getRealPath());
        //dd($img);
        $img->resize(120, 120, function ($constraint) {
            $constraint->aspectRatio();
        });
        // $path = $request->file('avatar')->store($img, 'public');
            $img->stream(); // <-- Key point
            //$path = $image->storeAs('images', $img);
            // dd('image size', $img);
            Storage::disk('public')->put($fileName, $img, 'public');
        //  }
        
        $Storeimage =FileUpload::create(['img'=>$fileName]);
       
        if ($Storeimage) {
            return redirect('/imagesdisplay');
        }
    }


    public function images()
    {
        return view('admin.upload.imges', ['images'=>Fileupload::all()]);
    }


    public function imagepath($imagename)
    {
        return Storage::get($imagename);
    }



    public function uploadtodbbkup(Request $request)
    {
        if ($request->hasFile('photo')) {
            $image      = $request->file('photo');
            $fileName   = time() . '.' . $image->getClientOriginalExtension();
            $img = Image::make($image->getRealPath());
            //dd($img);
            $img->resize(120, 120, function ($constraint) {
                $constraint->aspectRatio();
            });
            
            // $path = $request->file('avatar')->store($img, 'public');
            $img->stream(); // <-- Key point
            //$path = $image->storeAs('images', $img);
            // dd('image size', $img);
            Storage::disk('local')->put($fileName, $img, 'public');
        }
    }
    public function uploadtodbddd(Request $request)
    {
        if ($request->hasFile('photo')) {
            //$path = Storage::path('file.jpg');
            $validatedData = $request->validate([
                'photo' => 'required|image|mimes:jpg,png,jpeg,gif,svg|max:2048',
        
               ]);
        
            $name = $request->file('photo')->getClientOriginalName();
        
            //$path = $request->file('photo')->store('public/images');
        
            dd();
            // $save = new Photo;
        
            // $save->name = $name;
            // $save->path = $path;
        
            // $save->save();
            $file = $request->file('file');

            // Generate a file name with extension
            $fileName = 'profile-'.time().'.'.$file->getClientOriginalExtension();
        
            // Save the file
            $path = $file->storeAs('files', $fileName);

            dd();










            $file = $request->file('photo');
 
            // generate a new filename. getClientOriginalExtension() for the file extension
            $filename = 'profile-photo-' . time() . '.' . $file->getClientOriginalExtension();
 
            // save to storage/app/photos as the new $filename
            $path = $file->storeAs('photos', $filename);

            $img = Image::make($file->getRealPath());
            $img->resize(200, 200, function ($constraint) {
                $constraint->aspectRatio();
            });
            dd($img);
            $image      = $request->file('photo');
            $fileName   = time() . '.' . $image->getClientOriginalExtension();
            $img = Image::make($image->getRealPath());
            //dd($img);
            $img->resize(120, 120, function ($constraint) {
                $constraint->aspectRatio();
            });
            
            // $path = $request->file('avatar')->store($img, 'public');
            $img->stream(); // <-- Key point

            Storage::disk('public')->put($fileName, $img, 'public');
        }
    }
}
