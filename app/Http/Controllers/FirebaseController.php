<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\URL;
use Illuminate\Http\Request;

class FirebaseController extends Controller
{
    public function index()
    {
        ///dd(URL::asset('/img/firebasenotification.png'));
        return view('admin.firebase.index');
    }

    ///  dr8x1t-Q1Sg:APA91bH9WOhgFE8Kv9IUHh0Qw5XtWTTX-oioVvlqrsLfWhQ_hIW6yy3bB8Pj1ghY1tJJzsYp4kV1vNeDsd6b29KBGlPqldPRvNShmntPnigJM3RD8RJjyUjMz3tLg5wMcr1sEWJ_2I44

    /// notification

    public function notification()
    {
        $token = "dr8x1t-Q1Sg:APA91bH9WOhgFE8Kv9IUHh0Qw5XtWTTX-oioVvlqrsLfWhQ_hIW6yy3bB8Pj1ghY1tJJzsYp4kV1vNeDsd6b29KBGlPqldPRvNShmntPnigJM3RD8RJjyUjMz3tLg5wMcr1sEWJ_2I44";
        $from = "AAAAMr7-nVA:APA91bGAWp24dxMtj6xYKeev6FFAGgddIBlSNkvlK7z3Bpt5uk5lWiXESIw8dRXP89s7CcCY4h6AzvZ_Xm6GqbPToUIMg_D7f1gnJu18nO6Omp3XuXHOE7vhi9E0I0RraQYBq5VS2ygo";
        $msg = array(
                'body'  => "Testing Testing",
                'title' => "Hi, From Rajak Sources",
                'receiver' => 'erw',
                'icon'  => "https://image.flaticon.com/icons/png/512/270/270014.png",/*Default Icon*/
                'sound' => 'mySound'/*Default sound*/
              );

        $fields = array(
                    'to' => $token,
                    'notification'  => $msg
                );

        $headers = array(
                    'Authorization: key=' . $from,
                    'Content-Type: application/json'
                );
        //#Send Reponse To FireBase Server
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, 'https://fcm.googleapis.com/fcm/send');
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fields));
        $result = curl_exec($ch);
        // dd($result);
        curl_close($ch);
    }
}
