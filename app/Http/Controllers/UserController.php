<?php

namespace App\Http\Controllers;

use App\Exports\UserMultiSheetExport;
use App\Models\User;
use App\Models\Address;
use App\Exports\UsersExport;
use Maatwebsite\Excel\Excel;

class UserController extends Controller
{
    private $excel;

    public function __construct(Excel $excel)
    {
        $this->excel = $excel;
    }

    public function singleAddressuserlist()
    {
        $users = User::with('address_one')->get();
        //dd($users);
        return view('admin.user.address', compact('users'));
    }

    public function Addressuserlist()
    {
        $users = User::has('addresses', '>=', 2)->with('addresses')
        ->get();
        //whose user have more than one address
        return view('admin.user.addresses', compact('users'));
    }

    public function fileExport()
    {
        //return Excel::download(new UsersExport, 'users-collection.xlsx');
        //return $this->excel->download(new UsersExport, 'users-collection.xlsx');
        return $this->excel->download(new UserMultiSheetExport(2021), 'users-collection.xls');
        //return $this->excel->download(new UserMultiSheetExport(2020), 'users-collection.xls', Excel::XLS);
        //return $this->excel->download(new UsersExport, 'users-collection.pdf', Excel::DOMPDF);
    }

    public function bckupfileExport()
    {
        return Excel::download(new UsersExport, 'all_user');
    }
}
