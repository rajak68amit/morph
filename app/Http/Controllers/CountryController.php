<?php

namespace App\Http\Controllers;

use App\Events\UserCreated;
use App\Jobs\MailSend;
use App\Models\City;
use App\Models\User;
use App\Models\Country;
use App\Models\State;
use App\Jobs\MatchSendEmail;    
use App\Notifications\TaskCompleted;
use Illuminate\Notifications\Notifiable;
use Illuminate\Http\Request;

class CountryController extends Controller
{
    use Notifiable;

    public function Completedtask()
    {
        User::first()->notify(new TaskCompleted);
        return 'hello ';
    }

    public function UserCreated()
    {
        event(new UserCreated('Email has been send to your email Address'));
    }

    public function store()
    {
        $data = request()->validate([
            'email' => 'required|email'
        ]);

        /// ContestEntry::create($data);

        //event(NewEntryReceived::class);
    }

    public function sendEmail()
    {
        $data = [
            'email' => 'amitkumarrajak68@gmail.com'
        ];
        dispatch(new MailSend($data))->delay(now()->addMinutes(1));
        //$emailJob = new MatchSendEmail();

        //ddd($emailJob);
        //dispatch($emailJob);
    }

    public function countriesdetails()
    {
        return view('admin.country.details', ['countries' => Country::with('state', 'cities')->get()]);
    }

    public function inserlt()
    {
        $country = Country::create([
            'name' => 'India'
        ]);
        $country2 = Country::create([
            'name' => 'DUBAI'
        ]);
        $country3 = Country::create([
            'name' => 'CANADA'
        ]);

        $state1 = State::create([
            'c_id' => $country->id,
            'name' => 'Bihar',
        ]);
        $state2 = State::create([
            'c_id' => $country2->id,
            'name' => 'NASA',
        ]);
        $state3 = State::create([
            'c_id' => $country3->id,
            'name' => 'Beriaya',
        ]);

        City::create([
            's_id' => $state1->id,
            'name' => 'siwan'
        ]);
        City::create([
            's_id' => $state2->id,
            'name' => 'oly'
        ]);
        City::create([
            's_id' => $state3->id,
            'name' => 'bhema'
        ]);
    }
}
