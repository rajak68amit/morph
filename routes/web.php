<?php

use Illuminate\Support\Facades\Route;

Route::get('/', function () {
    return view('welcome');
});
Route::get('/fcm', 'FirebaseController@index');
Route::get('/notification', 'FirebaseController@notification')->name('notification');
Route::get('/address', 'UserController@singleAddressuserlist');
Route::get('/addresses', 'UserController@Addressuserlist');
Route::get('/fileExport', 'UserController@fileExport');
Route::get('/user_address', 'AddressController@index');
Route::get('/post_address', 'AddressController@indexd');
Route::get('/posts', 'PostController@index');
Route::get('/posttag', 'PostController@posttag');
Route::get('/tagpost', 'PostController@tagpost');
Route::get('/posttagattach', 'PostController@posttagattach');
Route::get('/posttagdetach', 'PostController@posttagdetach');
Route::get('/posttagsync', 'PostController@posttagsync');
Route::get('/tagpostattach', 'PostController@tagpostattach');
Route::get('/tagpostdetach', 'PostController@tagpostdetach');
Route::get('/tagpostsync', 'PostController@tagpostsync');
Route::get('/projectinsert', 'ProjectsController@index');
Route::get('/project', 'ProjectsController@show');
Route::get('/projectsTasks', 'ProjectsController@projectsTasks');
Route::get('/insertcountry', 'CountryController@inserlt');
Route::get('/countriesdetails', 'CountryController@countriesdetails');
Route::get('/sendEmail', 'CountryController@sendEmail');
Route::get('/UserCreated', 'CountryController@UserCreated');
Route::get('/TaskCompleted', 'CountryController@Completedtask');
Route::get('/projectTask', 'ProjectsController@projectTask');
Route::get('/comments', 'PostController@comments');
Route::get('/morphtag', 'PostController@morphtag');
Route::get('/ymail_front_matter', 'PostController@ymail_front_matter');

// Route::get('ymail-front-matter', function () {
//     $document = YamlFrontMatter::parseFile(
//         resource_path('views/ymal_front_matter.html')
//     );

//     ddd($document->excert);
// });

Auth::routes(['verify' => true]); // this is use for verificatin from email link

Route::get('/imagesdisplay', 'FileUploadController@images')->name('images');
Route::get('/fileupload', 'FileUploadController@upload')->name('fileupload');
Route::post('/uploadtodb', 'FileUploadController@uploadtodb')->name('uploadtodb');
Route::get('/home', 'HomeController@index')->name('home');
Route::get('/firebase', 'HomeController@firebase')->name('firebase');
Route::post('/save-token', 'HomeController@saveToken')->name('save-token');
Route::post('/send-notification', 'HomeController@sendNotification')->name('send.notification');
