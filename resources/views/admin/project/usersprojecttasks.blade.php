@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header" style="background-color: cyan;">{{ __('Projects Tasks') }}</div>
                <div class="card-body">
                    <div class="table-responsive">
                        <table class="table table-bordered">
                            <thead>
                                <th>#</th>
                                <th>Project</th>
                                <th>Tasks Name</th>
                            </thead>
                            <thead>
                                @foreach ($projects as $project)
                                <tr>
                                    <th>{{ $project->id }}</th>
                                    <th>{{ $project->name}}</th>
                                    <th>{{ $project->tasks->title}}</th>

                                    {{-- <th> @foreach ($project->tasks as $usr)
                                        <ol> {{ $usr->title}}</ol>
                                    @endforeach
                                    </th> --}}

                                    @endforeach
                                </tr>
                            </thead>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection