@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header" style="background-color: chartreuse;">{{ __('User Post on Tag') }}
                </div>

                <div class="card-body">
                    <div class="table-responsive">
                        <table class="table table-bordered">
                            <thead>
                                <th>#</th>
                                <th>Post</th>
                                <th>User name</th>
                                <th>Tag</th>
                            </thead>
                            <thead>

                                @foreach ($posts as $post)
                                <tr>
                                    <th>{{ $post->id }}</th>
                                    <th>{{ $post->title }}</th>
                                    <th>{{ $post->user->name}}</th>
                                    <th>
                                        @foreach ($post->tags as $tag)
                                        <ol>{{ $tag->name }} </br> {{ $tag->pivot->created_at }}</ol>
                                        @endforeach
                                    </th>
                                    @endforeach
                                </tr>

                            </thead>

                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection