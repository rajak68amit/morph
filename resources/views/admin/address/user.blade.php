@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{ __('Register') }}</div>

                <div class="card-body">
                    <div class="table-responsive">
                        <table class="table table-bordered">
                            <thead>
                                <th>#</th>
                                <th>Address</th>
                                <th>Name</th>
                            </thead>
                            <thead>
                                @foreach ($addresses as $address)
                                <tr>
                                    <th>{{ $address->id }}</th>
                                    <th>{{ $address->address }}</th>
                                    <th> {{ $address->user->name}}</th>
                                    {{-- <th> @foreach ($address->user as $u)
                                        @endforeach
                                    </th> --}}
                                    @endforeach
                                </tr>

                            </thead>

                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection