@extends('layouts.app')

@section('content')

<form action="{{ route('uploadtodb') }}" method="POST" enctype="multipart/form-data">
    @csrf
    <input id="file" type="file" class="@error('file') is-invalid @enderror" name="photo" value="{{ old('file') }}">
    @error('photo') <span style="color: red"> {{ $message }} </span> @enderror
    <button type="submit"> submit</button>

</form>

@endsection