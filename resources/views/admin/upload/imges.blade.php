@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header" style="background-color: cyan;">{{ __('Image Data ') }}</div>
                <div class="card-body">
                    <div class="table-responsive">
                        <table class="table table-bordered">
                            <thead>
                                <th>#</th>
                                <th>images</th>
                            </thead>
                            <thead>
                                @foreach ($images as $image)
                                <tr>
                                    <th>{{ $image->id }}</th>
                                    <th> <img src="{{ asset("/storage/$image->img") }}" alt="" title="">
                                    </th>
                                    @endforeach
                                </tr>
                            </thead>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection