<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>push notification</title>

    <!-- firebase integration started -->

    <script src="https://www.gstatic.com/firebasejs/5.5.9/firebase.js"></script>
    <!-- Firebase App is always required and must be first -->
    {{-- <script src="https://www.gstatic.com/firebasejs/8.6.7/firebase-app.js"></script> --}}

    <!-- Add additional services that you want to use -->

    {{-- <script src="https://www.gstatic.com/firebasejs/5.5.9/firebase-database.js"></script> --}}
    {{-- <script src="https://www.gstatic.com/firebasejs/5.5.9/firebase-firestore.js"></script> --}}
    {{-- <script src="https://www.gstatic.com/firebasejs/5.5.9/firebase-messaging.js"></script> --}}
    {{-- <script src="https://www.gstatic.com/firebasejs/5.5.9/firebase-functions.js"></script> --}}

    <!-- firebase integration end -->

    <!-- Comment out (or don't include) services that you don't want to use -->
    <!-- <script src="https://www.gstatic.com/firebasejs/5.5.9/firebase-storage.js"></script> -->

    {{-- <script src="https://www.gstatic.com/firebasejs/5.5.9/firebase.js"></script> --}}
    <script src="https://www.gstatic.com/firebasejs/8.6.7/firebase-analytics.js"></script>

</head>

<body>
    <h2><a href="{{ route('notification') }}"> notification</a></h2>
</body>
<script type="text/javascript">
    // Your web app's Firebase configuration
    var firebaseConfig = {
    apiKey: "AIzaSyDBHZmw1laPDuwjbJX16JKq17DfYb69gAQ",
    authDomain: "laravelfcm-31038.firebaseapp.com",
    projectId: "laravelfcm-31038",
    storageBucket: "laravelfcm-31038.appspot.com",
    messagingSenderId: "217952722256",
    appId: "1:217952722256:web:d5d3b0c21f86b3f95d2055",
    measurementId: "G-E0S5PJE4JF"
  };
// Initialize Firebase
firebase.initializeApp(firebaseConfig);
  //firebase.analytics();
//firebase.analytics();
const messaging = firebase.messaging();
	messaging
.requestPermission()
.then(function () {
//MsgElem.innerHTML = "Notification permission granted." 
	console.log("Notification permission granted.");

     // get the token in the form of promise
	return messaging.getToken()
})
.then(function(token) {
 // print the token on the HTML page     
  console.log('This is token=',token);
})
.catch(function (err) {
	console.log("Unable to get permission to notify.", err);
});

messaging.onMessage(function(payload) {
    console.log('payload', payload);
      /// you can received all perameter here 
      console.log("dhekho here!===============================")
    var notify;
    notify = new Notification(payload.notification.title,{
        body: payload.notification.body,
        icon: payload.notification.icon,
        tag: "Dummy"
    });  //show notification
    console.log('noti',payload.notification);
});
 
    //firebase.initializeApp(config);
var database = firebase.database().ref().child("/users/");
   
database.on('value', function(snapshot) {
    renderUI(snapshot.val());
});

// On child added to db
database.on('child_added', function(data) {
	console.log("Comming");
    if(Notification.permission!=='default'){
        var notify;
         
        notify= new Notification('CodeWife - '+data.val().username,{
            'body': data.val().message,
            'icon': 'bell.png',
            'tag': data.getKey()
        });
        notify.onclick = function(){
            alert(this.tag);
        }
    }else{
        alert('Please allow the notification first');
    }
});

self.addEventListener('notificationclick', function(event) {       
    event.notification.close();
});

</script>

</html>