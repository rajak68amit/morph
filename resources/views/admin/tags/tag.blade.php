@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header" style="background-color: cyan;">{{ __('User Tags on Post') }}</div>
                <div class="card-body">
                    <div class="table-responsive">
                        <table class="table table-bordered">
                            <thead>
                                <th>#</th>
                                <th>Tag</th>
                                <th>Post</th>
                            </thead>
                            <thead>

                                @foreach ($tags as $tag)
                                <tr>
                                    <th>{{ $tag->id }}</th>
                                    <th>{{ $tag->name }}</th>
                                    <th>
                                        @foreach ($tag->posts as $post)
                                        <ol>{{ $post->title }}</ol>
                                        @endforeach
                                    </th>
                                    @endforeach
                                </tr>
                            </thead>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection