@extends('layouts.app')
@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{ __('Country Details') }}</div>
                <div class="card-body">
                    @foreach ($countries as $country)
                    <h1>{{ $country->name }}</h1>
                    @foreach ($country->state as $state)
                    <h2>State ={{ $state->name }}</h2>
                    @foreach ($country->cities as $city)
                    <p>City = {{$city->name }}</p>
                    @endforeach
                    @endforeach
                    @endforeach
                </div>
            </div>
        </div>
    </div>
    @endsection